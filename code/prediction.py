
# ABC Ride Monitoring Solution by Eric Yin - 2021 Aug
# 
# PoC Demo Codes for 
# Prediction of Taxi Transcation Abnormal Change
#

import keras
import keras.backend as K
import pandas as pd
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
from keras.utils import to_categorical
import re
import argparse
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix, recall_score, precision_score
from keras.layers.core import Activation
from keras.models import Sequential,load_model
from keras.layers import Dense, Dropout, LSTM
from keras.regularizers import l2
from sklearn.metrics import mean_squared_error

np.random.seed(1234)
PYTHONHASHSEED = 0

# Based on requirements, the concurrency is 10k drivers.
# Assume that there is 1 transcation activities data record for each for each 10 mins.
# 
# 10000 / 10 / 60 = 16.7 records / second (Let's use 10 records / second for easy calculation)
# 
# If PREDICATE_ITEMS = 60, it predicts abnormal changes in next 6 seconds.
# If PREDICATE_ITEMS = 600, it predicts abnormal change in next 60 second.

PREDICATE_ITEMS = 600

# LSTM model's encoding length for data's time series.
# If SEQUENCE_LENGTH = 10, it means it encodes 10 records as one.

SEQUENCE_LENGTH = 10
BATCH_SIZE= 2000
EPOCH_SIZE = 25

SEQUENCE_COLS = ['passenger_count',
                 'trip_distance',
                 'fare_amount','extra',
                 'mta_tax', 'tip_amount','tolls_amount','improvement_surcharge',
                 'total_amount','congestion_surcharge']

def data_pp(df_features):
    df_features.columns = ['VendorID', 'tpep_pickup_datetime',
                 'tpep_dropoff_datetime', 'passenger_count',
                 'trip_distance','RatecodeID',
                 'PULocationID','DOLocationID',
                 'payment_type','fare_amount','extra',
                 'mta_tax', 'tip_amount','tolls_amount','improvement_surcharge',
                 'total_amount','congestion_surcharge', 'city']

    df_features['tpep_pickup_datetime'] = pd.to_datetime(df_features['tpep_pickup_datetime'])
    df_features['tpep_dropoff_datetime'] = pd.to_datetime(df_features['tpep_dropoff_datetime'])
    df_features = df_features.sort_values(by=['tpep_dropoff_datetime'], ascending=[True])

    df_features['city'] = 1 #'NewYork'

    df_features.drop('tpep_pickup_datetime', axis=1, inplace=True)
    df_features.drop('tpep_dropoff_datetime', axis=1, inplace=True)

    return df_features

def gen_sequence(id_df,seq_cols):
    # Only sequences that meet the window-length are considered, 
    # no padding is used. This means for testing
    # we need to drop those which are below the window-length. 

    # Generate sequence data.
    # If we have 1000 transcation data, SEQUENCE_LENGTH=10
    # so zip iterate over two following list of numbers
    # 0 10 -> from row 0 to row 10
    # 1 11 -> from row 1 to row 11
    # 2 12 -> from row 2 to row 12
    # ...

    data_matrix = id_df[seq_cols].values
    num_elements = data_matrix.shape[0] - PREDICATE_ITEMS - 1
    for start, stop in zip(range(0, num_elements-SEQUENCE_LENGTH), range(SEQUENCE_LENGTH, num_elements)):
        yield data_matrix[start:stop, :]

def gen_labels(id_df, label):
    # Generate training output sequence data.
    #       Output Sequence      Input Sequence
    # 0：   id_df[11, :]   ->    id_df[0:10, :]
    # 1：   id_df[12, :]   ->    id_df[1:11, :]
    # 2：   id_df[13, :]   ->    id_df[2:12, :]
    # 3：   id_df[14, :]   ->    id_df[3:13, :]

    data_matrix = id_df[label].values
    num_elements = data_matrix.shape[0] - PREDICATE_ITEMS
    return data_matrix[SEQUENCE_LENGTH+1:num_elements, :]

def gen_sequence_test(id_df, seq_cols):
    data_matrix = id_df[seq_cols].values
    num_elements = data_matrix.shape[0] - 1
    for start, stop in zip(range(num_elements-PREDICATE_ITEMS-SEQUENCE_LENGTH, num_elements-SEQUENCE_LENGTH), range(num_elements-PREDICATE_ITEMS, num_elements)):
        yield data_matrix[start:stop, :]

def gen_labels_test(id_df, label):
    data_matrix = id_df[label].values
    num_elements = data_matrix.shape[0]
    return data_matrix[num_elements-PREDICATE_ITEMS:num_elements, :]

def gen_sequence_infer(id_df, seq_cols):
    start_offset = -PREDICATE_ITEMS-SEQUENCE_LENGTH
    data_matrix = id_df[seq_cols].values
    for start, stop in zip(range(start_offset, -SEQUENCE_LENGTH-1), range(-PREDICATE_ITEMS, -1)):
        yield data_matrix[start:stop, :]

def r2_keras(y_true, y_pred):
    SS_res =  K.sum(K.square( y_true - y_pred ))
    SS_tot = K.sum(K.square( y_true - K.mean(y_true) ) )
    return ( 1 - SS_res/(SS_tot + K.epsilon()) )

#######
# TRAIN
#######
def train():

    # Step#1:
    # We build a deep network. 
    # The first layer is an LSTM layer with 100 units followed by another LSTM layer with 50 units. 
    # Dropout is also applied after each LSTM layer to control overfitting. 

    train_features_output_path = os.getcwd() + '/train_data/train_data.csv'
    train_df_features = pd.read_csv(train_features_output_path, sep=",", header=None)    
    train_df = data_pp(train_df_features)

    train_df_ = train_df
    train_table_list = train_df_['city'].unique()
    for name in train_table_list:
       print('train name: {}'.format(name))
    seq_gen = (list(gen_sequence(train_df_,  SEQUENCE_COLS))
       for city in train_table_list  if not train_df_[(train_df_['city']==city)].empty)
    seq_array = np.concatenate(list(seq_gen)).astype(np.float32)    
    print("seq_array shape: {}".format(seq_array.shape))
    
    label_gen = [gen_labels(train_df_[(train_df_['city']==city)], SEQUENCE_COLS)
                 for city in train_table_list if not train_df_[(train_df_['city']==city)].empty]
    label_array = np.concatenate(label_gen).astype(np.float32)
    print("label_array shape: {}".format(label_array.shape))

    nb_features = seq_array.shape[2]
    nb_out = label_array.shape[1]
    model = Sequential()
    model.add(LSTM(
              input_shape=(SEQUENCE_LENGTH, nb_features),
              units=100,
              return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(
              units=50,
              return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(units=nb_out, activation="relu"))
    model.compile(loss='mean_squared_error', optimizer='rmsprop',metrics=['mae',r2_keras])
    print(model.summary())

    model_path = os.getcwd() + '/models/prediction_model.h5'
    history = model.fit(seq_array, label_array, shuffle=True, epochs=EPOCH_SIZE, batch_size=BATCH_SIZE, validation_split=0.1, verbose=2,
              callbacks = [keras.callbacks.ModelCheckpoint(model_path,monitor='val_loss', save_best_only=False, mode='min', verbose=0)]
              )
    print(history.history.keys())

    # Output r2_keras graph
    fig_acc = plt.figure(figsize=(10, 10))
    plt.plot(history.history['r2_keras'])
    plt.plot(history.history['val_r2_keras'])
    plt.title('model r^2')
    plt.ylabel('R^2')
    plt.xlabel('epoch')
    plt.legend(['train', 'validate'], loc='upper left')
    plt.show()
    fig_acc.savefig(os.getcwd() + '/outputs/train/model_r2.png')

    # Output MAE graph
    fig_acc = plt.figure(figsize=(10, 10))
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['val_mean_absolute_error'])
    plt.title('model MAE')
    plt.ylabel('MAE')
    plt.xlabel('epoch')
    plt.legend(['train', 'validate'], loc='upper left')
    plt.show()
    fig_acc.savefig(os.getcwd() + '/outputs/train/model_mae.png')

    # Output loss graph
    fig_acc = plt.figure(figsize=(10, 10))
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validate'], loc='upper left')
    plt.show()
    fig_acc.savefig(os.getcwd() + '/outputs/train/model_regression_loss.png')

    scores = model.evaluate(seq_array, label_array, verbose=1, batch_size=BATCH_SIZE)
    print('\nMAE: {}'.format(scores[1]))
    print('\nR^2: {}'.format(scores[2]))

    y_pred = model.predict(seq_array,verbose=1, batch_size=BATCH_SIZE)
    validation_set = pd.DataFrame(y_pred, columns=SEQUENCE_COLS)
    validation_set['MSE'] = 0.
    validation_set.to_csv(os.getcwd() + '/outputs/train/predicate_train.csv', index = True)
    
######
# TEST
######
def test():
    test_features_output_path = os.getcwd() + '/test_data/test_data.csv'
    test_df_features = pd.read_csv(test_features_output_path, sep=",", header=None)
    test_df = data_pp(test_df_features)

    model_path = os.getcwd() + '/models/prediction_model.h5'
    if os.path.isfile(model_path):
        print('load_module: {}'.format(model_path))
        estimator = load_model(model_path,custom_objects={'r2_keras': r2_keras})
    
    for city in test_df['city'].unique():
        seq_array_test_last = (list(gen_sequence_test(test_df[(test_df['city']==city)], SEQUENCE_COLS))
            for city in test_df['city'].unique() if not test_df[(test_df['city']==city)].empty)
        seq_array_test_last = np.concatenate(list(seq_array_test_last)).astype(np.float32)
        print("seq_array_test_last shape: {}".format(seq_array_test_last.shape))

        label_array_test_last = [gen_labels_test(test_df[(test_df['city']==city)], SEQUENCE_COLS)
            for city in test_df['city'].unique() if not test_df[(test_df['city']==city)].empty]
        label_array_test_last = np.concatenate(label_array_test_last).astype(np.float32)
        label_array_test_last.shape
        print("label_array_test_last shape: {}".format(label_array_test_last.shape))

        scores_test = estimator.evaluate(seq_array_test_last, label_array_test_last, verbose=2)
        print('MAE: {}'.format(scores_test[1]))
        print('R^2: {}'.format(scores_test[2]))

        y_pred_test = estimator.predict(seq_array_test_last)
        y_true_test = label_array_test_last
        test_set = pd.DataFrame(y_pred_test, columns=SEQUENCE_COLS)

        test_set.to_csv(os.getcwd() + '/outputs/test/test.csv', index = None)

        fig_verify = plt.figure(figsize=(100, 50))
        plt.plot(y_pred_test, color="blue")
        plt.plot(y_true_test, color="red")
        plt.title('prediction')
        plt.ylabel('value')
        plt.xlabel('row')
        plt.legend(['predicted', 'actual data'], loc='upper left')
        plt.show()
        
        fig_verify.savefig(os.getcwd() + '/outputs/test/model_verify.png')

######
# INFERENCE
######
def inference():
    infer_features_output_path = os.getcwd() + '/inference_data/inference_data.csv'
    infer_df_features = pd.read_csv(infer_features_output_path, sep=",", header=None)
    infer_df = data_pp(infer_df_features)

    model_path = os.getcwd() + '/models/prediction_model.h5'
    if os.path.isfile(model_path):
        print('load_module: {}'.format(model_path))
        estimator = load_model(model_path,custom_objects={'r2_keras': r2_keras})
    for city in infer_df['city'].unique():
        infer_set = []
        seq_array_infer_last = [infer_df[SEQUENCE_COLS].values[-SEQUENCE_LENGTH:]]
        for i in range(PREDICATE_ITEMS):            
            if not len(infer_set) == 0:
                seq_array_infer_last = [seq_array_infer_last[0][1:]]
                seq_array_infer_last[0] = np.concatenate((seq_array_infer_last[0], infer_set[i-1]))

            seq_array_infer_last = np.asarray(seq_array_infer_last).astype(np.float32)
            y_pred_infer = estimator.predict(seq_array_infer_last)
            infer_set.extend([np.array(y_pred_infer).reshape(1, -1).astype(np.float32)])

        infer_set = pd.DataFrame(y_pred_infer, columns=SEQUENCE_COLS)
        infer_set.to_csv(os.getcwd() + '/outputs/inference/inference.csv', index = None)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", type=int, default=1,
            help="enable to train model, Default:1")
    parser.add_argument("--test", type=int, default=1,
            help="enable to test data based on train model, Default:1")
    parser.add_argument("--infer", type=int, default=0,
            help="enable to infer data based on train model, Default:0")
    args = parser.parse_args()
    if(args.train):
        train()
    if(args.test):
        test()
    if(args.infer):
        inference()
    exit(0)
