# ABC Ride Monitoring Solution by Eric Yin - 2021 Aug
# 
# # PoC Demo Codes for 
# Prediction of Taxi Transcation Abnormal Change
#

import argparse
import os
import warnings

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.exceptions import DataConversionWarning
from sklearn.compose import make_column_transformer

warnings.filterwarnings(action='ignore', category=DataConversionWarning)

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-test-split-ratio', type=float, default=0.3)
    parser.add_argument('--random-split', type=int, default=0)
    args, _ = parser.parse_known_args()
    
    print('Received arguments {}'.format(args))

    input_data_path = os.getcwd() + '/data/yellow_tripdata_2020-01.csv'
    
    print('Reading input data from {}'.format(input_data_path))
    df = pd.read_csv(input_data_path)

    df['city'] = 1 #'NewYork'

    df.drop('store_and_fwd_flag', axis=1, inplace=True)

    split_ratio = args.train_test_split_ratio
    random_state=args.random_split
    
    #split the data into train and test set
    train, test = train_test_split(df, test_size=split_ratio, random_state=random_state)
    
    #save the data
    train_features_headers_output_path = os.getcwd() + '/train_headers/train_data_with_headers.csv'
    
    train_features_output_path = os.getcwd() + '/train_data/train_data.csv'
    
    test_features_output_path = os.getcwd() + '/test_data/test_data.csv'
    
    print('Saving training features to {}'.format(train_features_output_path))
    train.to_csv(train_features_output_path, header=False, index=False)
    print("Complete")
    
    print("Save training data with headers to {}".format(train_features_headers_output_path))
    train.to_csv(train_features_headers_output_path, index=False)
                 
    print('Saving test features to {}'.format(test_features_output_path))
    test.to_csv(test_features_output_path, header=False, index=False)
    print("Complete")
