# Prediction Model for ABC Ride Monitoring Solution

## Setup Test Environment with Docker

* Make sure you have Docker environment in your machine, then run below commands

  * Build docker image
    `docker build -t abc-ml-model .`
  * Run docker
    `docker run -p 8080:8080 -it abc-ml-model`
  * Enter shell window of docker if you start a new terminal window
    `docker exec -it <instance-id> bash`

## Dataset

The raw data is data/yellow_tripdata_2020-01.csv.

Run `python3 ./code/preprocessing.py` to split it as Traning dataset and Test dataset.


| Type      | Filename                          |
| ----------- | ----------------------------------- |
| Training  | train_data/train_data.csv         |
| Test      | test_data/test_data.csv           |
| Inference | inference_data/inference_data.csv |

## Test Model

```
python3 code/prediction.py -h
optional arguments:
-h, --help  show this help message and exit
--train,    enable to train model, Default:1
--test,     enable to test data based on train model, Default:1
--infer,    enable to infer data based on train model, Default:0
```

Commands：

* How to train model
  `python3 ./code/prediction.py --train 1 --test 0 --infer 0`
* How to test trained model
  `python3 ./code/prediction.py --train 0 --test 1 --infer 0`
* How to do inference with the trained model
  `python3 ./code/prediction.py --train 0 --test 0 --infer`

## Outputs

* models/prediction_model.h5:  Trained model
* outputs/train/model_mae.png  : Model's MAE graph
* outputs/train/model_r2.png :  Model's R2 graph
* outputs/train/model_regression_loss.png :  Model's loss graph
* outputs/inference/inference.csv :  Predicted taxi transcation data
