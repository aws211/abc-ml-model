FROM ubuntu:18.04

RUN  sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list
RUN  apt-get clean
RUN apt update && \
    apt install -y git cmake && \
    apt install -y python3-pip && \
    python3 -m pip install --upgrade pip setuptools wheel -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com

RUN git clone https://gitlab.com/aws211/abc-ml-model.git

WORKDIR abc-ml-model

ENV PYTHONIOENCODING 'utf-8'
RUN python3 -m pip install -r requirement.txt  -i https://pypi.tuna.tsinghua.edu.cn/simple --ignore-installed pyxdg
